import org.junit.*;

public class FooTest {
    @Test
    public void charTest () {
        Assert.assertEquals('r', Foo.getR());
    }

    @Test
    public void intTest () {
        Assert.assertEquals(2, Foo.getTwo());
    }
}
